<?php
/**
 * @copyright Copyright © Novicell ApS. All rights reserved.
 * @license   proprietary
 * @link      https://www.novicell.dk/
 */
declare(strict_types=1);

namespace Novicell\Command;

use Symfony\Component\Console\{
    Attribute\AsCommand,
    Command\Command,
    Input\InputInterface,
    Output\OutputInterface
};
use Symfony\Component\Filesystem\{
    Exception\FileNotFoundException,
    Filesystem
};
use Symfony\Component\HttpClient\HttpClient;
use Symfony\Contracts\HttpClient\Exception\ClientExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\RedirectionExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\ServerExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface;
use Throwable;

#[AsCommand(
    name: 'selfupdate',
    description: 'Update tool to newest version',
    aliases: ['self-update', 'update']
)]
class SelfUpdateCommand extends Command
{
    private const RELEASES_URL = 'https://gitlab.com/api/v4/projects/37133802/releases';
    private const PHAR_DOWNLOAD_URL = 'https://gitlab.com/api/v4/projects/37133802/packages/generic/novicell-dev-tools/%s/novicell.phar';

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $httpClient = HttpClient::create();
        try {
            $response = $httpClient->request('GET', self::RELEASES_URL);
        } catch (Throwable $exception) {
            $output->writeln('<error>' . $exception->getMessage() . '</error>');

            return Command::FAILURE;
        }
        try {
            $responseContent = json_decode($response->getContent(), true, 512, JSON_THROW_ON_ERROR);
        } catch (Throwable $exception) {
            $output->writeln('<error>' . $exception->getMessage() . '</error>');

            return Command::FAILURE;
        }
        if (!is_array($responseContent) || !array_key_exists(0, $responseContent)) {
            $output->writeln('<error>Wrong response from Gitlab</error>');

            return Command::FAILURE;
        }
        $latestVersion = !empty($responseContent[0]) && is_array($responseContent[0]) ? $responseContent[0]['tag_name'] : '';
        if (!version_compare((string)$this->getApplication()?->getVersion(), $latestVersion, '<')) {
            $output->writeln('<info>No new update</info>');

            return Command::SUCCESS;
        }
        $currentFileName = $_SERVER['argv'][0] ?? '';
        if (!$currentFileName) {
            $output->writeln('<error>I have no idea what went wrong. Ask your tech lead</error>');

            return Command::FAILURE;
        }
        $fileSystem = new Filesystem();
        if (!$fileSystem->exists($currentFileName)) {
            throw new FileNotFoundException();
        }
        $output->writeln('<info>Downloading newest version...</info>');
        try {
            $content = $httpClient->request('GET', trim(sprintf(self::PHAR_DOWNLOAD_URL, $latestVersion)))->getContent();
        } catch (Throwable) {
            $output->writeln('<error>Something went wrong while downloading newest version</error>');

            return Command::FAILURE;
        }
        try {
            $fileSystem->dumpFile($currentFileName, $content);
        } catch (Throwable $exception) {
            $output->writeln('<error>' . $exception->getMessage() . '</error>');

            return Command::FAILURE;
        }
        $output->writeln('<info>You are now running the newest version</info>');

        return Command::SUCCESS;
    }
}
