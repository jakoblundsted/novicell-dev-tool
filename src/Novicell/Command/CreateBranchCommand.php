<?php
/**
 * @copyright Copyright © Novicell ApS. All rights reserved.
 * @license   proprietary
 * @link      https://www.novicell.dk/
 */
declare(strict_types=1);

namespace Novicell\Command;

use Novicell\Config;
use Novicell\Jira\Task;
use Symfony\Component\Console\{
    Attribute\AsCommand,
    Command\Command,
    Input\InputArgument,
    Input\InputInterface,
    Input\InputOption,
    Output\OutputInterface
};
use Symfony\Component\Process\Process;
use function Laravel\Prompts\confirm;
use function Laravel\Prompts\select;
use function Laravel\Prompts\spin;
use function Laravel\Prompts\text;

#[AsCommand(
    name: 'git:branch:create',
    description: 'Creates a new branch from task ID',
    aliases: ['gb', 'cb']
)]
class CreateBranchCommand extends Command
{
    protected function configure(): void
    {
        $this->addArgument('task_id', InputArgument::OPTIONAL, 'Why didn\'t you paste a task id?', '');
        $this->addOption('title', 't', InputOption::VALUE_OPTIONAL, 'Task title?');
        $this->setHelp('If you need to edit credentials, ask your tech lead for help. Remember to bring a Corny bar');
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $taskId = $input->getArgument('task_id');
        if (!is_string($taskId)) {
            $taskId = '';
        }
        if ($taskId === '') {
            $taskId = text(
                label: 'Task ID',
                placeholder: 'E.g. zxy-123',
                validate: fn(string $value) => match (true) {
                    !preg_match('/^\p{L}+-+\d+$/u', $value) => 'Please enter a valid task id',
                    default => null,
                },
                hint: 'This can be lowercase or uppercase',
            );
        }
        $taskTitle = $input->getOption('title');
        if (!is_string($taskTitle)) {
            $taskTitle = '';
        }
        $taskId = strtoupper($taskId);
        $task = new Task($output, $taskId);
        if (!$taskTitle) {
            $taskTitle = $task->getTitle();
            if (!$taskTitle) {
                $output->writeln('<error>Something went wrong, while generating task title!</error>');

                return Command::FAILURE;
            }
        }
        $jiraProjectSlug = (string)strtok($taskId, '-');
        $defaultBranch = (new Config())->getDefaultBranch($jiraProjectSlug);
        $taskType = $task->getTaskType();
        $output->writeln('<info>Fetching changes from bitbucket</info>');
        (new Process(['git', 'fetch', '--prune']))->run();
        $output->writeln('<info>Checking if branch exists</info>');
        $branchName = '';
        $gitLocalBranches = new Process(['git', 'branch']);
        $gitLocalBranches->run();
        if ($gitLocalBranches->getExitCode() !== 0) {
            $output->writeln('<error>' . $gitLocalBranches->getErrorOutput() . '</error>');

            return Command::FAILURE;
        }
        foreach (explode("\n", $gitLocalBranches->getOutput()) as $branch) {
            $branch = trim($branch);
            if (preg_match('/' . preg_quote($taskId, '/') . '(?!\d)/', $branch)) {
                $branchName = $branch;
                break;
            }
        }
        $gitRemoteBranches = new Process(['git', 'branch', '-r']);
        $gitRemoteBranches->run();
        if ($gitRemoteBranches->getExitCode() !== 0) {
            $output->writeln('<error>' . $gitRemoteBranches->getErrorOutput() . '</error>');

            return Command::FAILURE;
        }
        $existRemote = false;
        foreach (explode("\n", $gitRemoteBranches->getOutput()) as $branch) {
            $branch = trim($branch);
            if (preg_match('/' . preg_quote($taskId, '/') . '(?!\d)/', $branch)) {
                if (!$branchName) {
                    if (substr_count($branch, '/') > 1 && str_contains($branch, 'origin/')) {
                        $branchName = substr($branch, (int)strpos($branch, '/') + 1);
                    } else {
                        $branchName = substr($branch, (int)strpos($branch, $taskId));
                    }
                }
                $existRemote = true;
                break;
            }
        }
        if ($branchName) {
            $output->writeln('<info>Branch exist. Switching to branch</info>');
            $gitSwitchBranch = new Process(['git', 'switch', $branchName]);
            $gitSwitchBranch->run();
            if ($gitSwitchBranch->getExitCode() !== 0) {
                $output->writeln('<error>' . $gitSwitchBranch->getErrorOutput() . '</error>');

                return Command::FAILURE;
            }
            if ($existRemote) {
                $output->writeln('<info>Pulling changes from remote branch</info>');
                $gitPull = new Process(['git', 'pull', 'origin', $branchName]);
                $gitPull->run();
                if ($gitPull->getExitCode() !== 0) {
                    $output->writeln('<error>' . $gitPull->getErrorOutput() . '</error>');

                    return Command::FAILURE;
                }
            }
            $output->writeln('<info>Pulling changes from ' . $defaultBranch . ' into existing branch...</info>');
            $gitPullOrigin = new Process(['git', 'pull', 'origin', $defaultBranch]);
            $gitPullOrigin->run();
            if ($gitPullOrigin->getExitCode() !== 0) {
                $output->writeln('<error>' . $gitPullOrigin->getErrorOutput() . '</error>');

                return Command::FAILURE;
            }
        } else {
            $output->writeln('<comment>Branch does not exists</comment>');
            $output->writeln('<comment>Switching to ' . $defaultBranch . ' branch</comment>');
            $gitCheckout = new Process(['git', 'switch', $defaultBranch]);
            $gitCheckout->run();
            if ($gitCheckout->getExitCode() !== 0) {
                $output->writeln('<error>' . $gitCheckout->getErrorOutput() . '</error>');

                return Command::FAILURE;
            }
            $output->writeln('<info>Updating ' . $defaultBranch . ' branch with the newest changes from remote</info>');
            $gitPullOrigin = new Process(['git', 'pull', 'origin', $defaultBranch]);
            $gitPullOrigin->run();
            if ($gitPullOrigin->getExitCode() !== 0) {
                $output->writeln('<error>' . $gitPullOrigin->getErrorOutput() . '</error>');

                return Command::FAILURE;
            }
            $branchPrefix = select(
                label: 'Which prefix to you want to use?',
                options: ['feature', 'hotfix', 'none'],
                default: 'feature',
            );
            if ($branchPrefix === 'none') {
                $branchPrefix = '';
            }
            $cleanTitle = strtolower(trim((string)preg_replace('/-{2,}/', '-', (string)preg_replace('/[^A-Za-z0-9]+/', '-', $taskTitle)), '-'));
            $branchName = $branchPrefix . '/' . $taskId . '-' . $cleanTitle;
            $output->writeln('<info>Creating new branch</info>');
            $gitCheckoutNewTaskBranch = new Process(['git', 'switch', '-c', $branchName]);
            $gitCheckoutNewTaskBranch->run();
            if ($gitCheckoutNewTaskBranch->getExitCode() !== 0) {
                $output->writeln('<error>' . $gitCheckoutNewTaskBranch->getErrorOutput() . '</error>');

                return Command::FAILURE;
            }
        }
        if (($taskType === Task::JIRA_ISSUE_TYPE_TASK) && !$task->isInDevelopment() && confirm('Move task to \'' . Task::JIRA_SUBTASK_START_PROGRESS_STATUS . '\'?')) {
            $task->moveTask(Task::TASK_STATUS_IN_PROGRESS);
        } else {
            $output->writeln('<info>Task is already in progress</info>');
        }
        if ($taskType === Task::JIRA_ISSUE_TYPE_STORY) {
            $subTasks = [0 => 'Skip'];
            $key = 1;
            foreach ($task->getSubTasks() as $subTask) {
                $subTasks[$key++] = $subTask->fields->summary;
            }
            $subtaskTitle = select(label: 'Please select a subtask (Press \'Enter\' to skip)', options: $subTasks);
            foreach ($task->getSubTasks() as $subTask) {
                if ($subTask->fields->summary !== $subtaskTitle) {
                    continue;
                }
                $taskId = $subTask->key;
                $subTask = $task->getSubTask($taskId);
                if (
                    is_object($subTask) &&
                    isset($subTask->fields) &&
                    $subTask->fields->status->name !== Task::JIRA_SUBTASK_IN_PROGRESS_STATUS &&
                    confirm('Move subtask to \'' . Task::JIRA_SUBTASK_IN_PROGRESS_STATUS . '\'?')
                ) {
                    $task->moveSubtask(Task::JIRA_SUBTASK_IN_PROGRESS_STATUS, $taskId);
                }
                break;
            }
        }
        $output->writeln("\xF0\x9F\x8D\xBA \xF0\x9F\x8D\xBA " . '<info>Done</info>' . " \xF0\x9F\x8D\xBA \xF0\x9F\x8D\xBA");

        return Command::SUCCESS;
    }
}
