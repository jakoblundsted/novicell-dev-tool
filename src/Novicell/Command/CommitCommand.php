<?php
/**
 * @copyright Copyright © Novicell ApS. All rights reserved.
 * @license   proprietary
 * @link      https://www.novicell.dk/
 */
declare(strict_types=1);

namespace Novicell\Command;

use Novicell\{
    Bitbucket\Branch,
    Config,
    Jira\Task
};
use Symfony\Component\Console\{
    Attribute\AsCommand,
    Command\Command as SymfonyCommand,
    Helper\QuestionHelper,
    Input\InputArgument,
    Input\InputInterface,
    Input\InputOption,
    Output\OutputInterface,
    Question\ChoiceQuestion,
    Question\ConfirmationQuestion,
    Question\Question,
    Style\SymfonyStyle
};
use Symfony\Component\Process\Process;
use function Laravel\Prompts\select;

#[AsCommand(
    name: 'git:commit',
    description: 'Commits changes with correct branch prefix',
    aliases: ['gc', 'commit']
)]
class CommitCommand extends SymfonyCommand
{
    private const ARGUMENT_MESSAGE = 'message';
    private const OPTION_MESSAGE = self::ARGUMENT_MESSAGE;
    private const OPTION_PUSH = 'push';
    private const OPTION_NO_VERIFY = 'no-verify';

    protected function configure(): void
    {
        $this->addArgument(self::ARGUMENT_MESSAGE, InputArgument::OPTIONAL, 'Commit message');
        $this->addOption(self::OPTION_MESSAGE, 'm', InputOption::VALUE_OPTIONAL, 'Commit message');
        $this->addOption(self::OPTION_PUSH, 'p', InputOption::VALUE_NONE, 'Push branch');
        $this->addOption(self::OPTION_NO_VERIFY, 'n', InputOption::VALUE_NONE, 'Ignore git hooks');
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $gitBranchCurrent = new Process(['git', 'rev-parse', '--abbrev-ref', 'HEAD']);
        $gitBranchCurrent->run();
        if ($gitBranchCurrent->getExitCode() !== 0) {
            return SymfonyCommand::FAILURE;
        }
        $gitBranchCurrentName = trim($gitBranchCurrent->getOutput());
        preg_match('/[a-zA-Z]+-\d+/', $gitBranchCurrentName, $branchPrefix);
        $taskId = $branchPrefix[0];
        if (!$taskId || !preg_match('/^\p{L}+-+\d+$/u', $taskId)) {
            $output->writeln('<error>Something went wrong while trying to get prefix</error>');

            return SymfonyCommand::FAILURE;
        }
        $fixVersion = '';
        $task = new Task($output, $taskId);
        $fixVersions = $task->getIssue()->fields->fixVersions;
        if (!empty($fixVersions)) {
            $fixVersion = $fixVersions[0]->name;
        }
        $commitMessage = $input->getArgument(self::ARGUMENT_MESSAGE) ?: $input->getOption(self::OPTION_MESSAGE);
        if (!$commitMessage) {
            $questionHelper = new QuestionHelper();
            while (!$commitMessage) {
                $commitMessage = $questionHelper->ask($input, $output, new Question('Commit message: '));
            }
        }
        if (!is_string($commitMessage)) {
            $output->writeln('<error>Commit message wrong format</error>');

            return SymfonyCommand::FAILURE;
        }
        $gitCommitMessage = '[' . $taskId . '] ' . $commitMessage;
        $gitCommitArguments = ['git', 'commit', '-m', $gitCommitMessage];
        if ($input->getOption(self::OPTION_NO_VERIFY)) {
            $gitCommitArguments[] = '-n';
        }
        $gitCommit = new Process($gitCommitArguments);
        $gitCommit->setTty(true);
        $gitCommit->run();
        if ($gitCommit->getExitCode() !== 0) {
            return SymfonyCommand::FAILURE;
        }
        $symfonyStyle = new SymfonyStyle($input, $output);
        $symfonyStyle->block($commitMessage, $taskId, 'fg=black;bg=green', ' ', true);
        $questionHelper = new QuestionHelper();
        if ($input->getOption(self::OPTION_PUSH) || $questionHelper->ask($input, $output, new ConfirmationQuestion('Do you want to push to current branch? (Y/n) ', true))) {
            $output->writeln('<info>Pushing branch...</info>');
            $gitPush = new Process(['git', 'push', 'origin', $gitBranchCurrentName]);
            $gitPush->run();
            if ($gitPush->getExitCode() !== 0) {
                $output->writeln('<error>' . $gitPush->getErrorOutput() . '</error>');

                return SymfonyCommand::FAILURE;
            }
            $output->writeln('<info>Push successfully!</info>');
            $bitbucketBranch = (new Branch((new Config())->extractGitRepoName()));
            $releaseBranch = $bitbucketBranch->getReleaseBranch($fixVersion);
            if ($releaseBranch) {
                $targetBranch = '';
                if (is_string($releaseBranch)) {
                    $output->writeln('Found this release branch: ' . $releaseBranch);
                    if ($questionHelper->ask($input, $output, new ConfirmationQuestion('Create merge request to that release branch? (Y/n) ', true))) {
                        $targetBranch = $releaseBranch;
                    }
                }
                $choices = null;
                if (is_array($releaseBranch)) {
                    $output->writeln('<error>Could not find a release branch matching the fix version of the issue!</error>');
                    $output->writeln('Found these branches instead: ' . implode(', ', $releaseBranch));
                    $releaseBranch = ['skip', ...$releaseBranch];
                    $choices = new ChoiceQuestion('Which release branch do you want to create merge request to (Enter for Skip)?', $releaseBranch, 0);
                }
                if ($choices !== null && is_array($releaseBranch)) {
                    $targetBranch = select(
                        label: 'Which release branch do you want to create merge request to?',
                        options: $releaseBranch,
                        default: 'skip',
                        hint: 'The role may be changed at any time.'
                    );
                }
                if ($targetBranch && is_string($targetBranch)  && $targetBranch !== 'skip') {
                    $bitbucketBranch->createMergeRequest(
                        $gitBranchCurrentName,
                        $targetBranch,
                        $gitCommitMessage,
                        $output
                    );
                }
            }
        }
        $output->writeln("\xF0\x9F\x8D\xBA \xF0\x9F\x8D\xBA " . '<info>Done</info>' . " \xF0\x9F\x8D\xBA \xF0\x9F\x8D\xBA");

        return SymfonyCommand::SUCCESS;
    }
}
