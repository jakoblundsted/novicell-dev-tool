<?php
/**
 * @copyright Copyright © Novicell ApS. All rights reserved.
 * @license   proprietary
 * @link      https://www.novicell.dk/
 */
declare(strict_types=1);

namespace Novicell\Command;

use JsonException;
use Novicell\Config;
use Novicell\Jira\Task;
use Symfony\Component\Console\{
    Attribute\AsCommand,
    Command\Command,
    Helper\QuestionHelper,
    Input\InputInterface,
    Output\OutputInterface,
    Question\ChoiceQuestion,
    Question\ConfirmationQuestion
};
use Symfony\Component\HttpClient\HttpClient;
use Symfony\Component\Process\Process;
use Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface;

#[AsCommand(
    name: 'git:push',
    description: 'Push changes to current task branch',
    aliases: ['gp', 'push']
)]
class PushCommand extends Command
{
    private const BITBUCKET_API_URL = 'https://api.bitbucket.org/2.0/repositories/novicell/';

    protected function configure(): void
    {
        $this->addOption('no-merge', null, null, 'Do not create merge request(s)');
        $this->addOption('silent', null, null, 'Be silent');
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $gitBranchCurrent = new Process(['git', 'rev-parse', '--abbrev-ref', 'HEAD']);
        $gitBranchCurrent->run();
        if ($gitBranchCurrent->getExitCode() !== 0) {
            return Command::FAILURE;
        }
        $gitBranchCurrentName = trim($gitBranchCurrent->getOutput());
        preg_match('/[a-zA-Z]+-\d+/', $gitBranchCurrentName, $branchPrefix);
        $taskId = $branchPrefix[0];
        $jiraProjectSlug = (string)strtok($taskId, '-');
        $gitPush = new Process(['git', 'push', 'origin', $gitBranchCurrentName]);
        $gitPush->setTty(true);
        $gitPush->run();
        if ($gitPush->getExitCode() !== 0) {
            $output->writeln('<error>' . $gitPush->getErrorOutput() . '</error>');

            return Command::FAILURE;
        }
        if ($jiraProjectSlug === 'CARL' && !$input->getOption('silent')) {
            $output->write($gitPush->getOutput());

            return Command::SUCCESS;
        }
        $output->writeln('<info>Push successfully!</info>');
        if ($input->getOption('no-merge')) {
            return Command::SUCCESS;
        }
        $config = new Config();
        $gitProjectSlug = $config->extractGitRepoName();
        $output->writeln('<info>Checking for existing pull requests...</info>');
        $destinationBranches = $config->getDestinationBranches($jiraProjectSlug);
        $pullRequestLinks = [];
        foreach ($destinationBranches as $destination) {
            $httpClient = HttpClient::create();
            $response = $httpClient->request(
                'GET',
                self::BITBUCKET_API_URL . $gitProjectSlug . '/pullrequests',
                [
                    'headers' => [
                        'Authorization: Basic ' . base64_encode($config->getBitbucketUsername() . ':' . $config->getBitbucketToken()),
                        'Content-Type: application/json'
                    ],
                ]
            );
            if ($response->getStatusCode() !== 200) {
                $output->writeln('<error>Something went wrong, status code: ' . $response->getStatusCode() . '</error>');
                $output->writeln('<error>' . $response->getContent(false) . '</error>');

                return Command::FAILURE;
            }
            $pullRequestResponse = json_decode($response->getContent(false), true, 512, JSON_THROW_ON_ERROR);
            if (!is_array($pullRequestResponse)) {
                $output->writeln('<error>Something went wrong, response is not an array</error>');

                return Command::FAILURE;
            }
            $pullRequests = $pullRequestResponse['values'] ?? [];
            foreach ($pullRequests as $pullRequest) {
                if ($pullRequest['source']['branch']['name'] === $gitBranchCurrentName && $pullRequest['destination']['branch']['name'] === $destination) {
                    $pullRequestLink = $pullRequest['links']['html']['href'] ?? '';
                    $output->writeln('<info>Found existing pull request to </info><options=bold>' . ucfirst($destination) . '</><info>:</info>');
                    $output->writeln('<options=underscore>' . ($pullRequestLink) . '</>');
                    $pullRequestLinks[] = $pullRequestLink;
                    $destinationBranches = array_diff($destinationBranches, [$destination]);
                }
            }
        }
        $questionHelper = new QuestionHelper();
        if (!empty($destinationBranches)) {
            if (
                !$questionHelper->ask(
                    $input,
                    $output,
                    new ConfirmationQuestion('Do you want to create a new merge request to ' . implode(' & ', $destinationBranches) . '? (y/n) ', false)
                )
            ) {
                $output->writeln("\xF0\x9F\x8D\xBA \xF0\x9F\x8D\xBA " . '<info>Done</info>' . " \xF0\x9F\x8D\xBA \xF0\x9F\x8D\xBA");

                return Command::SUCCESS;
            }
            $gitLatestCommitMessage = new Process(['git', 'log', '-1', '--pretty=%B']);
            $gitLatestCommitMessage->run();
            if ($gitLatestCommitMessage->getExitCode() !== 0) {
                return Command::FAILURE;
            }
            $reviewers = [];
            if ($config->getBitbucketUsername() !== 'novicell_jln') {
                $reviewers[] = ['username' => 'novicell_jln'];
            }
            foreach ($destinationBranches as $destination) {
                $pullRequest = [
                    'title' => trim($gitLatestCommitMessage->getOutput()),
                    'source' => [
                        'branch' => [
                            'name' => $gitBranchCurrentName
                        ]
                    ],
                    'destination' => [
                        'branch' => [
                            'name' => $destination
                        ]
                    ],
                    'reviewers' => $reviewers
                ];
                $httpClient = HttpClient::create();
                try {
                    $response = $httpClient->request(
                        'POST',
                        self::BITBUCKET_API_URL . $gitProjectSlug . '/pullrequests',
                        [
                            'headers' => [
                                'Authorization: Basic ' . base64_encode($config->getBitbucketUsername() . ':' . $config->getBitbucketToken()),
                                'Content-Type: application/json'
                            ],
                            'body' => json_encode($pullRequest, JSON_THROW_ON_ERROR)
                        ]
                    );
                    if ($response->getStatusCode() !== 201) {
                        $output->writeln('<error>Something went wrong, status code: ' . $response->getStatusCode() . '</error>');
                        $output->writeln('<error>' . $response->getContent(false) . '</error>');

                        return Command::FAILURE;
                    }
                    $pullRequestResponse = json_decode($response->getContent(), true, 512, JSON_THROW_ON_ERROR);
                    if (!is_array($pullRequestResponse)) {
                        continue;
                    }
                    $output->writeln('<options=bold>' . ucfirst($destination) . ':</>');
                    $output->writeln('<options=underscore>' . ($pullRequestResponse['links']['html']['href'] ?? '') . '</>');
                } catch (TransportExceptionInterface | JsonException $exception) {
                    $output->writeln('<error>' . $exception->getMessage() . '</error>');

                    return Command::FAILURE;
                }
            }
        }
        if ($questionHelper->ask($input, $output, new ConfirmationQuestion('Do you want to open the pull requests in your browser? (y/n) '))) {
            foreach ($pullRequestLinks as $pullRequestLink) {
                $process = new Process(['open', $pullRequestLink]);
                $process->run();
            }
        }
        $task = new Task($output, $taskId);
        $taskType = $task->getTaskType();
        if (
            ($taskType === Task::JIRA_ISSUE_TYPE_TASK) && !$task->isInQa() && $questionHelper->ask(
                $input,
                $output,
                new ConfirmationQuestion('Move task to \'' . Task::JIRA_QA_STATUS . '\'? (y/n) ', false)
            )
        ) {
            $task->moveTaskToQa();
        }
        $output->writeln("\xF0\x9F\x8D\xBA \xF0\x9F\x8D\xBA " . '<info>Done</info>' . " \xF0\x9F\x8D\xBA \xF0\x9F\x8D\xBA");

        return Command::SUCCESS;
    }
}
