<?php
/**
 * @copyright Copyright © Novicell ApS. All rights reserved.
 * @license   proprietary
 * @link      https://www.novicell.dk/
 */
declare(strict_types=1);

namespace Novicell\Command\Aws;

use Novicell\Config;
use Symfony\Component\Console\Attribute\AsCommand;
use Symfony\Component\Console\Command\Command as SymfonyCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;
use Symfony\Component\Process\Process;
use function Laravel\Prompts\select;

#[AsCommand(
    name: 'aws:cron',
    description: 'Connect to cron container on AWS',
)]
class CronCommand extends SymfonyCommand
{
    private const array PROJECT_SLUG_MAPPING = [
        'carletti' => 'carl',
    ];
    private const string OPTION_STAGING = 'staging';
    private const string OPTION_PRODUCTION = 'production';

    #[\Override]
    protected function configure(): void
    {
        $this->addOption(self::OPTION_STAGING, 'stag', InputOption::VALUE_NONE, 'Staging environment');
        $this->addOption(self::OPTION_PRODUCTION, 'prod', InputOption::VALUE_NONE, 'Production environment');
    }

    #[\Override]
    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $config = new Config();
        $gitProjectSlug = $config->extractGitRepoName();
        $gitProjectSlug = (string)strtok($gitProjectSlug, '-');
        if (!$gitProjectSlug) {
            $output->writeln('<error>Something went wrong while trying to get prefix</error>');

            return SymfonyCommand::FAILURE;
        }
        $projectSlug = self::PROJECT_SLUG_MAPPING[$gitProjectSlug] ?? $gitProjectSlug;
        if ($projectSlug === 'bj-gear') {
            $output->writeln('<error>Project not supported</error>');

            return SymfonyCommand::SUCCESS;
        }
        $environment = (string)select(
            label: 'Which environment do you want to connect to?',
            options: [self::OPTION_STAGING, self::OPTION_PRODUCTION],
            default: self::OPTION_STAGING,
        );
        if ($environment === 'production') {
            $io = new SymfonyStyle($input, $output);
            $io->caution('-> PRODUCTION ENVIRONMENT DETECTED');
            if (!$io->confirm('Confirm that you want to connect to <error>PRODUCTION</error>', false)) {
                $output->writeln('<question>[AWS]</question> <info>Aborted</info>');

                return SymfonyCommand::SUCCESS;
            }
        }
        $output->writeln('<question>[AWS]</question> <info>Getting task ID for ' . $environment . ' cron container</info>');
        $awsSlug = $projectSlug . '-' . $environment;
        $process = new Process(['aws', 'ecs', 'list-tasks', '--cluster', $awsSlug . '-magento2', '--service-name', $awsSlug . '-cron-ecs-service', '--profile', $projectSlug]);
        $process->run();
        if ($process->getExitCode() !== 0) {
            $output->writeln('<error>' . $process->getErrorOutput() . '</error>');

            return SymfonyCommand::FAILURE;
        }
        $processOutput = explode('/', $process->getOutput());
        $taskId = trim(end($processOutput));
        $output->writeln('<question>[AWS]</question> <comment>Found ID: ' . $taskId . '</comment>');
        $output->writeln('<question>[AWS]</question> <info>Connecting to ' . $environment . ' cron container [' . $taskId . ']</info>');
        $phpContainerName = match ($projectSlug) {
            'carl' => 'cron',
            default => 'cron',
        };
        pcntl_signal(SIGINT, SIG_IGN);
        $process = new Process(['aws', 'ecs', 'execute-command', '--cluster', $awsSlug . '-magento2', '--container', $phpContainerName, '--command', '/bin/bash', '--interactive', '--task', $taskId, '--profile', $projectSlug]);
        $process->setTty(true);
        $process->setTimeout(null);
        $process->run();
        pcntl_signal(SIGINT, SIG_DFL);
        $output->writeln('<question>[AWS]</question> <info>Disconnected from ' . $environment . ' cron container [' . $taskId . ']</info>');

        return SymfonyCommand::SUCCESS;
    }
}
