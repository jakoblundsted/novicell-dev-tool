<?php
/**
 * @copyright Copyright © Novicell ApS. All rights reserved.
 * @license   proprietary
 * @link      https://www.novicell.dk/
 */
declare(strict_types=1);

namespace Novicell\Command;

use Symfony\Component\Console\{
    Attribute\AsCommand,
    Command\Command,
    Input\InputInterface,
    Output\OutputInterface
};
use Symfony\Component\Process\Process;

#[AsCommand(
    name: 'git:pull',
    description: 'Pull changes to current task branch',
    aliases: ['pull']
)]
class PullCommand extends Command
{
    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $gitBranchCurrent = new Process(['git', 'rev-parse', '--abbrev-ref', 'HEAD']);
        $gitBranchCurrent->run();
        if ($gitBranchCurrent->getExitCode() !== 0) {
            return Command::FAILURE;
        }
        $gitBranchCurrentName = trim($gitBranchCurrent->getOutput());
        $gitPush = new Process(['git', 'pull', 'origin', $gitBranchCurrentName]);
        $gitPush->setTty(true);
        $gitPush->run();
        if ($gitPush->getExitCode() !== 0) {
            $output->writeln('<error>' . $gitPush->getErrorOutput() . '</error>');

            return Command::FAILURE;
        }

        return Command::SUCCESS;
    }
}
