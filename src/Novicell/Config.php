<?php
/**
 * @copyright Copyright © Novicell ApS. All rights reserved.
 * @license   proprietary
 * @link      https://www.novicell.dk/
 */
declare(strict_types=1);

namespace Novicell;

use RuntimeException;
use Symfony\Component\Console\{
    Helper\QuestionHelper,
    Input\InputInterface,
    Output\OutputInterface,
    Question\Question
};
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\Process\Process;
use Throwable;

class Config
{
    private const CONFIG_FILE = '.config' . DIRECTORY_SEPARATOR . 'novicell.json';
    private const DEFAULT_BRANCH = 'master';
    private const DEFAULT_STAGING = 'staging';
    private const CREDENTIAL_USERNAME = 'username';
    private const CREDENTIAL_PASSWORD = 'password';
    private const CREDENTIAL_BITBUCKET_USERNAME = 'bitbucket_username';
    private const CREDENTIAL_BITBUCKET_TOKEN = 'bitbucket_token';
    private string $configFile;

    public function __construct()
    {
        $this->configFile = (getenv('HOME') ?: '~') . DIRECTORY_SEPARATOR . self::CONFIG_FILE;
    }

    public function checkCredentials(InputInterface $input, OutputInterface $output): void
    {
        $questionHelper = new QuestionHelper();
        $configData = $this->getConfigData();
        $updateConfig = false;
        $credentials = [
            self::CREDENTIAL_USERNAME => [
                'question' => 'Your initials (\'xyz\' - Used for Jira): ',
                'error' => 'Wrong initials',
                'callback' => function (string $value): string {
                    return trim($value) . '@novicell.dk';
                },
            ],
            self::CREDENTIAL_PASSWORD => [
                'question' => 'Your Atlassian API key (https://id.atlassian.com/manage-profile/security/api-tokens): ',
                'error' => 'Invalid password',
            ],
            self::CREDENTIAL_BITBUCKET_USERNAME => [
                'question' => 'Your Bitbucket username (https://bitbucket.org/account/settings/ - Look under \'Bitbucket profile settings\'): ',
                'error' => 'Invalid bitbucket username',
            ],
            self::CREDENTIAL_BITBUCKET_TOKEN => [
                'question' => 'Your Bitbucket app password (https://bitbucket.org/account/settings/app-passwords/ - Needs: Repositories[Read|Write], Pull requests[Read|Write]): ',
                'error' => 'Invalid bitbucket app password',
            ],
        ];
        foreach ($credentials as $key => $credential) {
            if (!empty($configData[$key])) {
                continue;
            }
            $value = $this->askQuestion($input, $output, $questionHelper, $credential['question'], $credential['error']);
            $configData[$key] = !empty($credential['callback']) ? $credential['callback']($value) : trim($value);
            $updateConfig = true;
        }
        if ($updateConfig) {
            $this->setConfigData($configData);
        }
    }

    private function askQuestion(InputInterface $input, OutputInterface $output, QuestionHelper $questionHelper, string $questionText, string $errorMessage): string
    {
        $answer = $questionHelper->ask($input, $output, new Question($questionText));
        if (!is_string($answer)) {
            throw new RuntimeException($errorMessage);
        }

        return $answer;
    }

    private function setConfigData(mixed $configData): void
    {
        try {
            file_put_contents($this->getConfigFile(), json_encode($configData, JSON_THROW_ON_ERROR));
        } catch (Throwable) {
            throw new RuntimeException('Could not write to config file');
        }
    }

    public function getConfigFile(): string
    {
        $fileSystem = new Filesystem();
        if (!$fileSystem->exists($this->configFile)) {
            $fileSystem->touch($this->configFile);
        }
        if (!$fileSystem->exists($this->configFile)) {
            throw new RuntimeException('Could not create config file');
        }

        return $this->configFile;
    }

    /**
     * To create PR's to
     * @return string[]
     */
    public function getDestinationBranches(string $jiraProjectSlug): array
    {
        return match ($jiraProjectSlug) {
            'INW' => [self::DEFAULT_STAGING, 'production-live'],
            'CARL', 'BELIMO' => [self::DEFAULT_BRANCH],
            'GD' => [self::DEFAULT_STAGING, self::DEFAULT_BRANCH],
            default => [self::DEFAULT_STAGING]
        };
    }

    /**
     * To branch out from
     */
    public function getDefaultBranch(string $jiraProjectSlug): string
    {
        return match ($jiraProjectSlug) {
            'INW' => 'production-live',
            default => self::DEFAULT_BRANCH
        };
    }

    public function getBitbucketUsername(): string
    {
        return $this->getCredential(self::CREDENTIAL_BITBUCKET_USERNAME);
    }

    public function getBitbucketToken(): string
    {
        return $this->getCredential(self::CREDENTIAL_BITBUCKET_TOKEN);
    }

    public function getJiraUsername(): string
    {
        return $this->getCredential(self::CREDENTIAL_USERNAME);
    }

    public function getJiraPassword(): string
    {
        return $this->getCredential(self::CREDENTIAL_PASSWORD);
    }

    private function getCredential(string $credentialKey): string
    {
        $configData = $this->getConfigData();

        return $configData[$credentialKey] ?? '';
    }

    /**
     * @return string[]
     */
    private function getConfigData(): array
    {
        try {
            $configData = json_decode(file_get_contents($this->getConfigFile()) ?: '{}', true, 512, JSON_THROW_ON_ERROR);
        } catch (Throwable) {
            $configData = [];
        }

        return is_array($configData) ? $configData : [];
    }

    public function extractGitRepoName(): string
    {
        $gitRepoName = new Process(['git', 'config', '--get', 'remote.origin.url']);
        $gitRepoName->run();
        if ($gitRepoName->getExitCode() !== 0) {
            return '';
        }
        preg_match('/\/([a-zA-Z0-9_-]*)\.git/', $gitRepoName->getOutput(), $matches);
        if (!empty($matches) && count($matches) > 1) {
            return $matches[1];
        }

        return '';
    }
}
