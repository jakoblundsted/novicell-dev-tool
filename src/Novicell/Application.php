<?php
/**
 * @copyright Copyright © Novicell ApS. All rights reserved.
 * @license   proprietary
 * @link      https://www.novicell.dk/
 */
declare(strict_types=1);

namespace Novicell;

use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputDefinition;
use Symfony\Component\Console\Input\InputOption;

class Application extends \Symfony\Component\Console\Application
{
    private static string $name = 'Novicell - Development tool';
    private static string $version = '1.6.0';
    private static string $logo = <<<LOGO
         _   _            _          _ _
        | \ | |          (_)        | | |
        |  \| | _____   ___  ___ ___| | |
        | . ` |/ _ \ \ / / |/ __/ _ \ | |
        | |\  | (_) \ V /| | (_|  __/ | |
        \_| \_/\___/ \_/ |_|\___\___|_|_|

LOGO;

    public function __construct()
    {
        parent::__construct(self::$name, self::$version);
    }

    #[\Override]
    public function getHelp(): string
    {
        return self::$logo . parent::getHelp();
    }

    #[\Override]
    protected function getDefaultInputDefinition(): InputDefinition
    {
        return new InputDefinition([
            new InputArgument('command', InputArgument::REQUIRED, 'The command to execute'),
            new InputOption('--help', '-h', InputOption::VALUE_NONE, 'Display help for the given command. When no command is given display help for the <info>list</info> command'),
            new InputOption('--quiet', '-q', InputOption::VALUE_NONE, 'Do not output any message'),
            new InputOption('--verbose', '-v|vv|vvv', InputOption::VALUE_NONE, 'Increase the verbosity of messages: 1 for normal output, 2 for more verbose output and 3 for debug'),
            new InputOption('--version', '-V', InputOption::VALUE_NONE, 'Display this application version'),
            new InputOption('--ansi', '', InputOption::VALUE_NEGATABLE, 'Force (or disable --no-ansi) ANSI output', null),
        ]);
    }
}
