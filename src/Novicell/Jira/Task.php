<?php
/**
 * @copyright Copyright © Novicell ApS. All rights reserved.
 * @license   proprietary
 * @link      https://www.novicell.dk/
 */
declare(strict_types=1);

namespace Novicell\Jira;

use Exception;
use Novicell\Config;
use RuntimeException;
use stdClass;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\HttpClient\HttpClient;
use Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface;
use Throwable;

class Task
{
    public const TASK_STATUS_IN_PROGRESS = 'in_progress';
    public const JIRA_QA_STATUS = 'Ready to deploy to QA';
    public const JIRA_IN_PROGRESS_STATUS = 'In development';
    public const JIRA_SUBTASK_IN_PROGRESS_STATUS = 'In Progress';
    public const JIRA_SUBTASK_START_PROGRESS_STATUS = 'Start progress';
    public const JIRA_ISSUE_TYPE_TASK = 'Task';
    public const JIRA_ISSUE_TYPE_STORY = 'Story';
    private const JIRA_API_URL = 'https://novicell.atlassian.net/rest/api/latest/issue/';
    private ?stdClass $issue = null;

    public function __construct(
        private readonly OutputInterface $output,
        private readonly string $taskId
    ) {}

    public function getIssue(): stdClass
    {
        if ($this->issue) {
            return $this->issue;
        }
        $config = new Config();
        $httpClient = HttpClient::create();
        try {
            $response = $httpClient->request(
                'GET',
                self::JIRA_API_URL . $this->taskId,
                [
                    'headers' => [
                        'Accept: application/json',
                        'Authorization: Basic ' . base64_encode($config->getJiraUsername() . ':' . $config->getJiraPassword())
                    ]
                ]
            );
        } catch (TransportExceptionInterface $exception) {
            throw new RuntimeException($exception->getMessage());
        }
        $jiraStatusCode = $response->getStatusCode();
        if ($jiraStatusCode !== 200) {
            $errorMessage = match ($jiraStatusCode) {
                404 => 'Task not found!',
                401 => 'Wrong credentials!',
                default => 'Something went wrong?',
            };
            throw new RuntimeException($errorMessage);
        }
        $issue = json_decode($response->getContent());
        if (!$issue instanceof stdClass) {
            throw new RuntimeException('Issue is not stdClass');
        }

        return $this->issue = $issue;
    }

    public function isInQa(): bool
    {
        if (!isset($this->getIssue()->fields->status->name)) {
            throw new Exception('Status is not set');
        }

        return $this->getIssue()->fields->status->name === self::JIRA_QA_STATUS;
    }

    public function isInDevelopment(): bool
    {
        if (!isset($this->getIssue()->fields->status->name)) {
            throw new Exception('Status is not set');
        }

        return match ($this->getIssue()->fields->status->name) {
            self::JIRA_IN_PROGRESS_STATUS, self::JIRA_SUBTASK_IN_PROGRESS_STATUS => true,
            default => false,
        };
    }

    public function moveTaskToQa(): void
    {
        $this->moveTask(self::JIRA_QA_STATUS);
    }

    public function moveTask(string $status, string $taskId = null): void
    {
        $config = new Config();
        $httpClient = HttpClient::create();
        try {
            $response = $httpClient->request(
                'GET',
                self::JIRA_API_URL . '/' . ($taskId ?: $this->taskId) . '/transitions',
                [
                    'headers' => [
                        'Accept: application/json',
                        'Authorization: Basic ' . base64_encode($config->getJiraUsername() . ':' . $config->getJiraPassword())
                    ]
                ]
            );
            $jiraStatusCode = $response->getStatusCode();
        } catch (TransportExceptionInterface $exception) {
            $this->output->writeln('<error>' . $exception->getMessage() . '</error>');

            return;
        }
        if ($jiraStatusCode === 404) {
            $this->output->writeln('<error>Task not found!</error>');

            return;
        }
        if ($jiraStatusCode === 401) {
            $this->output->writeln('<error>Wrong credentials!</error>');

            return;
        }
        if ($jiraStatusCode !== 200) {
            $this->output->writeln('<error>Something went wrong (' . __METHOD__ . ')</error>');

            return;
        }
        try {
            $transitions = json_decode($response->getContent(), false, 512, JSON_THROW_ON_ERROR);
        } catch (Throwable $exception) {
            $this->output->writeln('<error>' . $exception->getMessage() . '</error>');

            return;
        }
        if (!is_object($transitions)) {
            $this->output->writeln('<error>Something went wrong, while getting task statuses.</error>');

            return;
        }
        if (empty($transitions->transitions)) {
            $this->output->writeln('<error>Something went wrong, while getting task statuses.</error>');

            return;
        }
        $transitionId = 0;
        foreach ($transitions->transitions as $transition) {
            if (($this->getTaskType() === self::JIRA_ISSUE_TYPE_STORY) && $transition->to->name === $status) {
                $transitionId = $transition->to->id;
                break;
            }
            if (
                $status === self::TASK_STATUS_IN_PROGRESS &&
                (
                    $transition->name === self::JIRA_SUBTASK_IN_PROGRESS_STATUS ||
                    $transition->name === self::JIRA_SUBTASK_START_PROGRESS_STATUS ||
                    $transition->name === self::JIRA_IN_PROGRESS_STATUS
                )
            ) {
                $transitionId = $transition->id;
                break;
            }
            if ($transition->name === $status) {
                $transitionId = $transition->id;
                break;
            }
        }
        if (!$transitionId) {
            $this->output->writeln('<error>Cannot get proper "In progress" status for task.</error>');
        }
        $httpClient = HttpClient::create();
        try {
            $response = $httpClient->request(
                'POST',
                self::JIRA_API_URL . ($taskId ?: $this->taskId) . '/transitions',
                [
                    'headers' => [
                        'Authorization: Basic ' . base64_encode($config->getJiraUsername() . ':' . $config->getJiraPassword()),
                        'Content-Type: application/json'
                    ],
                    'body' => json_encode(['transition' => ['id' => $transitionId]], JSON_THROW_ON_ERROR)
                ]
            );
            $jiraStatusCode = $response->getStatusCode();
        } catch (Throwable $exception) {
            $this->output->writeln('<error>' . $exception->getMessage() . '</error>');
        }
        if ($jiraStatusCode === 404) {
            $this->output->writeln('<error>Task not found!</error>');
        }
        if ($jiraStatusCode === 401) {
            $this->output->writeln('<error>Wrong credentials!</error>');
        }
        if ($jiraStatusCode !== 204) {
            $this->output->writeln('<error>Something went wrong - (' . __METHOD__ . ')</error>');
        }
    }

    public function getTitle(): string
    {
        if (!isset($this->getIssue()->fields->summary)) {
            throw new Exception('Summary is not set');
        }

        return $this->getIssue()->fields->summary ?? '';
    }

    public function getTaskType(): string
    {
        if (!isset($this->getIssue()->fields->issuetype->name)) {
            throw new Exception('Issue type is not set');
        }

        return $this->getIssue()->fields->issuetype->name ?? '';
    }

    /**
     * @return array|stdClass[]
     */
    public function getSubTasks(): array
    {
        if (!isset($this->getIssue()->fields->subtasks)) {
            throw new Exception('Subtasks are not set');
        }

        return $this->getIssue()->fields->subtasks ?? [];
    }

    public function getSubTask(string $issueId): ?stdClass
    {
        foreach ($this->getSubTasks() as $subTask) {
            if ($subTask->key === $issueId) {
                return $subTask;
            }
        }

        return null;
    }

    public function moveSubtask(string $status, string $issueId): void
    {
        foreach ($this->getSubTasks() as $subTask) {
            if ($subTask->key === $issueId && $status === self::JIRA_SUBTASK_IN_PROGRESS_STATUS) {
                $this->moveTask(self::JIRA_SUBTASK_START_PROGRESS_STATUS, $issueId);
                break;
            }
        }
    }
}
