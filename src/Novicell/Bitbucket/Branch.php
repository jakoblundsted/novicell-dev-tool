<?php
/**
 * @copyright Copyright © Novicell ApS. All rights reserved.
 * @license   proprietary
 * @link      https://www.novicell.dk/
 */
declare(strict_types=1);

namespace Novicell\Bitbucket;

use Novicell\Config;
use RuntimeException;
use stdClass;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\HttpClient\HttpClient;
use Throwable;

class Branch
{
    private const BITBUCKET_API_URL = 'https://api.bitbucket.org/2.0/repositories/novicell/';

    public function __construct(
        private readonly string $gitProjectSlug
    ) {}

    /**
     * @return string|array<string>
     */
    public function getReleaseBranch(string $fixVersion): string | array
    {
        $config = new Config();
        $httpClient = HttpClient::create();
        $response = $httpClient->request(
            'GET',
            self::BITBUCKET_API_URL . $this->gitProjectSlug . '/refs',
            [
                'headers' => [
                    'Authorization: Basic ' . base64_encode($config->getBitbucketUsername() . ':' . $config->getBitbucketToken()),
                    'Content-Type: application/json'
                ],
            ]
        );
        $bitbucketStatusCode = $response->getStatusCode();
        if ($bitbucketStatusCode !== 200) {
            $errorMessage = match ($bitbucketStatusCode) {
                404 => 'Branches not found!',
                401 => 'Wrong credentials!',
                403 => 'You do not have access to this!',
                default => 'Something went wrong?',
            };
            throw new RuntimeException($errorMessage);
        }
        try {
            $branches = json_decode($response->getContent(), false, 512, JSON_THROW_ON_ERROR);
        } catch (throwable $exception) {
            throw new RuntimeException($exception->getMessage());
        }
        if (!$branches instanceof stdClass) {
            throw new RuntimeException('Branches is not an object');
        }
        $releaseBranch = '';
        $allBranches = $branches->values;
        foreach ($allBranches as $branch) {
            if ($branch->name === 'release/' . $fixVersion) {
                $releaseBranch = $branch->name;
                break;
            }
        }
        if (!$releaseBranch) {
            $response = $httpClient->request(
                'GET',
                self::BITBUCKET_API_URL . $this->gitProjectSlug . '/branching-model',
                [
                    'headers' => [
                        'Authorization: Basic ' . base64_encode($config->getBitbucketUsername() . ':' . $config->getBitbucketToken()),
                        'Content-Type: application/json'
                    ],
                ]
            );
            $bitbucketStatusCode = $response->getStatusCode();
            if ($bitbucketStatusCode !== 200) {
                $errorMessage = match ($bitbucketStatusCode) {
                    404 => 'Branches not found!',
                    401 => 'Wrong credentials!',
                    403 => 'You do not have access to this!',
                    default => 'Something went wrong?',
                };
                throw new RuntimeException($errorMessage);
            }
            try {
                $branchingModel = json_decode($response->getContent(), false, 512, JSON_THROW_ON_ERROR);
            } catch (throwable $exception) {
                throw new RuntimeException($exception->getMessage());
            }
            if (!$branchingModel instanceof stdClass) {
                throw new RuntimeException('BranchingModel is not an object');
            }
            $targetBranches = [];
            /** @var stdClass[] $branchingModel */
            foreach ($branchingModel as $type => $branch) {
                if (!in_array($type, ['development', 'production'], true)) {
                    continue;
                }
                if ($branch->branch->name === 'release/' . $fixVersion) {
                    return $branch->branch->name;
                }
                $targetBranches[] = $branch->branch->name;
            }

            return $targetBranches;
        }

        return $releaseBranch;
    }

    public function createMergeRequest(string $sourceBranch, string $destinationBranch, string $gitCommitMessage, OutputInterface $output): void
    {
        $config = new Config();
        $httpClient = HttpClient::create();
        $response = $httpClient->request(
            'POST',
            self::BITBUCKET_API_URL . $this->gitProjectSlug . '/pullrequests',
            [
                'headers' => [
                    'Authorization: Basic ' . base64_encode($config->getBitbucketUsername() . ':' . $config->getBitbucketToken()),
                    'Content-Type: application/json'
                ],
                'body' => json_encode(
                    [
                        'title' => $gitCommitMessage,
                        'source' => [
                            'branch' => [
                                'name' => $sourceBranch
                            ]
                        ],
                        'destination' => [
                            'branch' => [
                                'name' => $destinationBranch
                            ]
                        ],
                        'close_source_branch' => false
                    ],
                    JSON_THROW_ON_ERROR
                )
            ]
        );
        $bitbucketStatusCode = $response->getStatusCode();
        if ($bitbucketStatusCode !== 201) {
            $errorMessage = match ($bitbucketStatusCode) {
                404 => 'Branches not found!',
                401 => 'Wrong credentials!',
                403 => 'You do not have access to this!',
                default => 'Something went wrong?',
            };
            throw new RuntimeException($errorMessage);
        }
        $pullRequest = json_decode($response->getContent(), false, 512, JSON_THROW_ON_ERROR);
        if (!is_object($pullRequest)) {
            throw new RuntimeException('Pull request is not an object');
        }
        $output->writeln('<options=bold>Bitbucket:</>');
        $output->writeln('<options=underscore>' . ($pullRequest->links->html->href ?? '') . '</>');
    }
}
